const log4js = require('log4js');

const logger = log4js.getLogger();
logger.level = 'error';

let x = 4;

logger.trace('el valor de la variable es ${x} ');
logger.debug('el valor de la variable es ${x} ');
logger.info('el valor de la variable es ${x} ');
logger.warn('el valor de la variable es ${x} ');
logger.error('el valor de la variable es ${x} ');
logger.fatal('el valor de la variable es ${x} ');

function sumar(x,y){
  return x+y;
}

module.exports = sumar;
