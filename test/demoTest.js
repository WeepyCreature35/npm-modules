const assert = require('assert');
const sumar = require('../app');

describe('probar la suma de numeros',()=>{
  it('cinco mas cinco es diez', ()=>{
    assert.equal(10, sumar(5,5));
  });
  it('cinco mas cinco son cincuenta y 5', ()=>{
    assert.notEqual("55", sumar(5,5));
  });
});
